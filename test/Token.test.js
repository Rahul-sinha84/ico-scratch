const Token = artifacts.require("Token.sol");
import chai from "./setupChai";

const expect = chai.expect;
const BN = web3.utils.BN;
contract("Token", async (accounts) => {
  const [deployer, recepient, anotherAcc, oneMoreAcc, fifthAcc] = accounts;
  const name = "Token";
  const symbol = "TKN";
  const totalTokens = 10000000000;
  let tokenInstance;
  before(async () => {
    tokenInstance = await Token.new(name, symbol, totalTokens);

    //transferring token first;
    const tokens = 100;
    await tokenInstance.initialTransfer(deployer, tokens);
    await tokenInstance.initialTransfer(oneMoreAcc, tokens);
  });

  describe("Deployed", () => {
    it("deployed successfully", async () => {
      const address = await tokenInstance.address;
      expect(address).to.be.not.equal(0x0);
      expect(address).to.be.not.equal("");
      expect(address).to.be.not.equal(null);
      return expect(address).to.be.not.equal(undefined);
    });
    it("input is correct", async () => {
      const totalSupply = await tokenInstance.totalSupply();
      expect(tokenInstance.name()).to.eventually.be.equal(name);
      expect(tokenInstance.symbol()).to.eventually.be.equal(symbol);
      return expect(totalSupply.toNumber()).to.be.equal(totalTokens);
    });
  });

  describe("ERC20 characteristics", () => {
    it("Allowance", async () => {
      const tokenValue = 100;
      const tokenSend = 10;
      await tokenInstance.approve(recepient, tokenValue);

      //transferring token from the allowed account
      await tokenInstance.transferFrom(deployer, anotherAcc, tokenSend, {
        from: recepient,
      });
      (
        await tokenInstance.allowance(deployer, recepient)
      ).should.be.a.bignumber.equal(new BN(tokenValue).sub(new BN(tokenSend)));
      (
        await tokenInstance.allowance(deployer, anotherAcc)
      ).should.be.a.bignumber.equal(new BN(0));
      // token should be deducted from owner's account

      (await tokenInstance.balanceOf(deployer)).should.be.a.bignumber.equal(
        new BN(tokenValue).sub(new BN(tokenSend))
      );
      //token should add up to the receiver account
      (await tokenInstance.balanceOf(anotherAcc)).should.be.a.bignumber.equal(
        new BN(tokenSend)
      );
    });
    it("Token transfer", async () => {
      const tokenSend = 10;
      await tokenInstance.transfer(recepient, tokenSend, {
        from: oneMoreAcc,
      });

      //token should be deducted from sender
      (await tokenInstance.balanceOf(oneMoreAcc)).should.be.a.bignumber.equal(
        new BN(new BN(100).sub(new BN(tokenSend)))
      );
      //token should add up to the receiver
      (await tokenInstance.balanceOf(recepient)).should.be.a.bignumber.equal(
        new BN(tokenSend)
      );
      //should not get fullfilled as there is no balance in this account
      return expect(
        tokenInstance.transfer(deployer, tokenSend, { from: fifthAcc })
      ).to.eventually.be.rejected;
    });
  });
});
