import chai from "./setupChai";
const Token = artifacts.require("Token.sol");
const TokenSale = artifacts.require("TokenSale.sol");
const MoneyCollector = artifacts.require("MoneyCollector.sol");

const expect = chai.expect;
const BN = web3.utils.BN;

contract("TokenSale contract", async (accounts) => {
  const [deployer, recepient, anotherAcc] = accounts;

  const name = "Token";
  const symbol = "TKN";
  const totalTokens = 10000000000;
  const rate = 1;
  let tokenInstance;
  let tokenSaleInstance;
  let moneyCollectorInstance;

  beforeEach(async () => {
    tokenInstance = await Token.new(name, symbol, totalTokens);

    moneyCollectorInstance = await MoneyCollector.new();

    tokenSaleInstance = await TokenSale.new(
      tokenInstance.address,
      rate,
      moneyCollectorInstance.address
    );

    //transferring all tokens to tokensale
    await tokenInstance.initialTransfer(tokenSaleInstance.address, totalTokens);
  });

  //for setting the kyc for 2nd account
  const setKYC = async () => {
    await tokenSaleInstance.approveKyc(recepient);
  };

  //for sending tokens from tokenSale contract to 2nd account
  const buyToken = async (tokenToBuy) => {
    setKYC();

    await tokenSaleInstance.buyToken(tokenToBuy, {
      from: recepient,
      value: tokenToBuy * rate,
    });
  };

  describe("Deployed", () => {
    it("deployed successfully", async () => {
      const address = await tokenSaleInstance.address;
      expect(address).to.be.not.equal(0x0);
      expect(address).to.be.not.equal("");
      expect(address).to.be.not.equal(null);
      return expect(address).to.be.not.equal(undefined);
    });
    it("inputs are stored", async () => {
      (await tokenSaleInstance.rate()).should.be.a.bignumber.equal(
        new BN(rate)
      );
    });
    it("all tokens are transferred to contract", async () => {
      const tokenSaleAddress = await tokenSaleInstance.address;
      (
        await tokenInstance.balanceOf(tokenSaleAddress)
      ).should.be.a.bignumber.equal(new BN(totalTokens));
    });
    it("Owner should matched", async () => {
      return expect(tokenSaleInstance.isOwner(deployer)).to.eventually.be.equal(
        true
      );
    });
  });

  describe("Tokens buying", () => {
    it("KYC", async () => {
      setKYC();
      //return kyc access to be true
      expect(tokenSaleInstance.checkKyc(recepient)).to.eventually.be.equal(
        true
      );
      //return kyc to be false
      expect(tokenSaleInstance.checkKyc(anotherAcc)).to.eventually.be.equal(
        false
      );
      //should be rejected if it is not called by owner
      return expect(
        tokenSaleInstance.approveKyc(anotherAcc, { from: recepient })
      ).to.eventually.be.rejected;
    });
    it("Buy token", async () => {
      const tokenToBuy = 10;
      const exceedToken = 100000000000;
      buyToken(tokenToBuy);

      //should be rejected if amount paid is wrong
      expect(
        tokenSaleInstance.buyToken(tokenToBuy, { from: recepient, value: 20 })
      ).to.eventually.be.rejected;
      //should be rejected if the token value reaches its limit
      expect(
        tokenSaleInstance.buyToken(exceedToken, {
          from: recepient,
          value: exceedToken,
        })
      ).to.eventually.be.rejected;
      //should be rejected from non-kyc member
      return expect(
        tokenSaleInstance.buyToken(tokenToBuy, { from: anotherAcc, value: 10 })
      ).to.eventually.be.rejected;
    });
    it("Balances left", async () => {
      const tokenToBuy = 30;
      buyToken(tokenToBuy);
      //buyer should now have tokens
      (await tokenInstance.balanceOf(recepient)).should.be.a.bignumber.equal(
        new BN(tokenToBuy)
      );
      //tokens should be deducted from tokenSale contract
      (
        await tokenInstance.balanceOf(tokenSaleInstance.address)
      ).should.be.a.bignumber.equal(
        new BN(totalTokens).sub(new BN(tokenToBuy))
      );
      //wei collected should increased
      (await tokenSaleInstance.totalFund()).should.be.a.bignumber.equal(
        new BN(tokenToBuy * rate)
      );
      //tokens sold should increased
      (await tokenSaleInstance.tokenSold()).should.be.a.bignumber.equal(
        new BN(tokenToBuy)
      );
      //eth collected in the moneyCollector contract should increase
      (
        await web3.eth.getBalance(moneyCollectorInstance.address)
      ).should.be.a.bignumber.equal(new BN(tokenToBuy * rate));
    });
    it("External payment to contract", async () => {
      const tokenToSend = 10;
      //setting kyc first
      setKYC();
      //sending eth externally
      await web3.eth.sendTransaction({
        from: recepient,
        value: tokenToSend,
        to: tokenSaleInstance.address,
        gas: "300000",
      });
      //token should be send to buyer's account
      (await tokenInstance.balanceOf(recepient)).should.be.a.bignumber.equal(
        new BN(tokenToSend)
      );
      //wei collected should be forwarded to moneyCollector contract
      (
        await web3.eth.getBalance(moneyCollectorInstance.address)
      ).should.be.a.bignumber.equal(new BN(tokenToSend * rate));
      //wei collected can be withdrawn by owner
      expect(moneyCollectorInstance.withdrawMoney(5)).to.eventually.be
        .fulfilled;
      //should not be fulfilled by account other than owner
      return expect(
        moneyCollectorInstance.withdrawMoney(5, { from: recepient })
      ).to.eventually.be.rejected;
    });
  });
});
