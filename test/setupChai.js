import chai from "chai";
import chaiAsBn from "chai-bn";
import chaiAsPromised from "chai-as-promised";
import { should } from "chai";

const BN = web3.utils.BN;
const chaiBN = chaiAsBn(BN);

chai.use(chaiAsPromised);
chai.use(chaiBN);
chai.use(should);

export default chai;
