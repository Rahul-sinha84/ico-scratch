const path = require("path");
require("babel-register");
require("babel-polyfill");
const dotenv = require("dotenv");
const HDWalletProvider = require("@truffle/hdwallet-provider");
dotenv.config();
const seedPhrase = process.env.SEED_PHRASE;
const metamaskAccountIndex = 0;

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    develop: {
      port: 8545,
      host: "localhost",
      network_id: "*",
    },
    ganache: {
      provider: () => {
        return new HDWalletProvider(
          seedPhrase,
          "http://127.0.0.1:7545",
          metamaskAccountIndex
        );
      },
      network_id: 5777,
    },
    goerli_infura: {
      provider: () => {
        return new HDWalletProvider(
          seedPhrase,
          "https://goerli.infura.io/v3/48121259b5e34df2a3b7196ccbf66edb",
          metamaskAccountIndex
        );
      },
      network_id: 5,
    },
    rinkby_infura: {
      provider: () => {
        return new HDWalletProvider(
          seedPhrase,
          "https://rinkeby.infura.io/v3/48121259b5e34df2a3b7196ccbf66edb",
          metamaskAccountIndex
        );
      },
      network_id: 4,
    },
    ropsten_infura: {
      provider: () => {
        return new HDWalletProvider(
          seedPhrase,
          "https://ropsten.infura.io/v3/48121259b5e34df2a3b7196ccbf66edb",
          metamaskAccountIndex
        );
      },
      network_id: 3,
    },
  },
  compilers: {
    solc: {
      version: "0.8.0",
    },
  },
};
