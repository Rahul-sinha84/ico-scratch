import React from "react";
import LoadWeb3Data from "./components/loadDetails";
import { BrowserRouter, Route } from "react-router-dom";
import Header from "./components/Header";
import About from "./components/About";
import HandleKyc from "./components/HandleKyc";
import WithdrawMoney from "./components/WithdrawMoney";
import BuyToken from "./components/BuyToken";
import { connect } from "react-redux";

const App = ({ state }) => {
  return (
    <>
      <BrowserRouter>
        <Route component={Header} path="/" />
        <Route component={LoadWeb3Data} path="/" />
        <Route component={BuyToken} path="/" exact />
        {state.isOwner ? (
          <>
            <Route component={About} path="/crowdSale" />
            <Route component={HandleKyc} path="/handleKyc" />
            <Route component={WithdrawMoney} path="/withdraw" />
          </>
        ) : null}
      </BrowserRouter>
    </>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps)(App);
