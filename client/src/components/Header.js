import React from "react";
import { Nav, Navbar, Container, NavDropdown } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { connect } from "react-redux";
const Header = ({ state }) => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
        <LinkContainer to="/">
          <Navbar.Brand>ICO</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {state.isOwner ? (
              <>
                <LinkContainer to="/crowdSale">
                  <Nav.Link>Your Crowdsale</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/withdraw">
                  <Nav.Link>Withdraw Money</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/handleKyc">
                  <Nav.Link>Handle KYCs</Nav.Link>
                </LinkContainer>
              </>
            ) : null}
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>{state.account.address}</Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps)(Header);
