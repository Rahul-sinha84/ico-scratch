import React, { useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap";
import { connect } from "react-redux";

const About = ({ state }) => {
  const [name, setName] = useState("");
  const [symbol, setSymbol] = useState("");
  const [totalSupply, setTotalSupply] = useState(null);
  const [rate, setRate] = useState(null);
  const [tokenSold, setTokenSold] = useState(null);
  const [totalMoneyCollected, setTotalMoneyCollected] = useState(null);
  const [availableMoney, setAvailableMoney] = useState(null);
  const [totalKyc, setTotalKyc] = useState(null);

  useEffect(() => {
    (async () => {
      await fetchData();
    })();
  }, [state]);

  const fetchData = async () => {
    //loading name
    if (state.tokenInstance.methods) {
      const tokenName = await state.tokenInstance.methods.name().call();
      setName(tokenName);
      const tokenSymol = await state.tokenInstance.methods.symbol().call();
      setSymbol(tokenSymol);
      const tokenTotalSupply = await state.tokenInstance.methods
        .totalSupply()
        .call();
      setTotalSupply(tokenTotalSupply);
    }
    if (state.tokenSaleInstance.methods) {
      const saleRate = await state.tokenSaleInstance.methods.rate().call();
      setRate(saleRate);
      const totalTokenSold = await state.tokenSaleInstance.methods
        .tokenSold()
        .call();
      setTokenSold(totalTokenSold);
      const totalFund = await state.tokenSaleInstance.methods
        .totalFund()
        .call();
      setTotalMoneyCollected(totalFund);
      const _totalKycs = await state.tokenSaleInstance.methods
        .totalKyc()
        .call({ from: state.account.address });
      setTotalKyc(_totalKycs);
    }
    if (state.moneyInstance.methods) {
      const _availableMoney = await state.moneyInstance.methods
        .totalMoney()
        .call();
      setAvailableMoney(_availableMoney);
    }
  };
  return (
    <Container className="mt-5">
      <Table striped bordered hover>
        <tbody>
          <tr>
            <td>Token Name</td>
            <td>{name}</td>
          </tr>
          <tr>
            <td>Token Symbol</td>
            <td>{symbol}</td>
          </tr>
          <tr>
            <td>Total Initial Supply</td>
            <td>{totalSupply} TKN</td>
          </tr>
          <tr>
            <td>Rate of Crowdsale</td>
            <td>{rate}</td>
          </tr>
          <tr>
            <td>Total Token Sold</td>
            <td>{tokenSold}</td>
          </tr>
          <tr>
            <td>Total KYCs</td>
            <td>{totalKyc}</td>
          </tr>
          <tr>
            <td>Total money collected so far</td>
            <td>{totalMoneyCollected} wei</td>
          </tr>
          <tr>
            <td>Available money in contract</td>
            <td>{availableMoney} wei</td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps)(About);
