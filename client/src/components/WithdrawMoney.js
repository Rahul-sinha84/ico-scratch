import React, { useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { connect } from "react-redux";

const WithdrawMoney = ({ state }) => {
  const [num, setNum] = useState("");
  const onSubmit = async (e) => {
    e.preventDefault();
    if (!num) return alert("Enter valid amount !!");
    //withdrawing
    await state.moneyInstance.methods
      .withdrawMoney(num)
      .send({ from: state.account.address })
      .then((res) => setNum(""));
  };
  return (
    <Container className="mt-5">
      <h4 className="text-center">Enter Amount to withdraw in 'Wei' </h4>
      <Form onSubmit={onSubmit}>
        <Form.Control
          style={{ width: "50%", display: "block", margin: "2rem auto" }}
          size="lg"
          type="number"
          value={num}
          onChange={(e) => setNum(e.target.value)}
        />
        <Button
          type="submit"
          style={{ display: "block", margin: "1rem auto" }}
          variant="primary"
        >
          Withdraw
        </Button>
      </Form>
    </Container>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });
export default connect(mapStateToProps)(WithdrawMoney);
