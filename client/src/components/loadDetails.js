import React, { useEffect, useState } from "react";
import { Alert } from "react-bootstrap";
import getWeb3 from "../getWeb3";
import Token from "../contracts/Token.json";
import TokenSale from "../contracts/TokenSale.json";
import MoneyCollector from "../contracts/MoneyCollector.json";
import { connect } from "react-redux";
import action from "../redux/actions";

const LoadDetails = ({
  state,
  changeToken,
  changeTokensale,
  changeMoney,
  changeOwner,
  changeWeb3,
  changeAccount,
}) => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    (async () => {
      let web3 = await getWeb3();
      loadWeb3(web3);
      setLoading(false);
    })();
  }, [state]);

  const getBal = async (tokenContract, address) => {
    //getting token balance
    const bal = await tokenContract.methods.balanceOf(address).call();
    return bal;
  };

  const loadWeb3 = async (web3) => {
    let accountToState = {};
    changeWeb3(web3);
    const accounts = await web3.eth.getAccounts();
    accountToState.address = accounts[0];

    const networkId = await web3.eth.net.getId();
    const networkDataToken = Token.networks[networkId];
    const networkDataTokenSale = TokenSale.networks[networkId];
    const networkDataMoneyCollctr = MoneyCollector.networks[networkId];
    if (networkDataToken && networkDataTokenSale && networkDataMoneyCollctr) {
      //setting token instance
      let tokenContract = await new web3.eth.Contract(
        Token.abi,
        networkDataToken.address
      );
      changeToken(tokenContract);

      //setting tokenSale instance
      let tokenSaleContract = await new web3.eth.Contract(
        TokenSale.abi,
        networkDataTokenSale.address
      );
      changeTokensale(tokenSaleContract);

      let moneyCollectorContract = await new web3.eth.Contract(
        MoneyCollector.abi,
        networkDataMoneyCollctr.address
      );
      changeMoney(moneyCollectorContract);
      //checking whether the current account is owner or not
      isOwner(tokenSaleContract, accounts[0]);
      //getting balance of current user
      accountToState.tokenBalance = await getBal(tokenContract, accounts[0]);
      changeAccount(accountToState);
    } else {
      return window.alert("Current App is not deployed to detected network !!");
    }
  };
  //for getting the owner...
  const isOwner = async (saleContract, address) => {
    const val = await saleContract.methods.isOwner(address).call();
    changeOwner(val);
  };

  if (loading) {
    return (
      <Alert
        variant="warning"
        style={{ width: "fit-content", margin: "1rem auto" }}
        className="text-center"
      >
        Loading...
      </Alert>
    );
  } else {
    return (
      <Alert
        variant="success"
        style={{ width: "fit-content", margin: "1rem auto" }}
        className="text-center"
      >
        Loading completed !!
      </Alert>
    );
  }
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps, action)(LoadDetails);
