import React, { useState, useEffect } from "react";
import { Button, Container, Form, Alert } from "react-bootstrap";
import { connect } from "react-redux";

const BuyToken = ({ state }) => {
  const [tokenAmount, setTokenAmount] = useState("");
  const [rate, setRate] = useState(null);
  const [kycApproved, setKycApproved] = useState(false);

  const onSubmit = async (e) => {
    e.preventDefault();
    if (!tokenAmount) return alert("Enter valid amount !!");

    await state.tokenSaleInstance.methods
      .buyToken(tokenAmount)
      .send({ from: state.account.address, value: `${tokenAmount * rate}` })
      .then((res) => setTokenAmount(""));
  };

  useEffect(() => {
    (async () => {
      await loadData();
    })();
  }, [state]);
  const loadData = async () => {
    if (state.tokenSaleInstance.methods && state.account.address) {
      const _rate = await state.tokenSaleInstance.methods.rate().call();
      setRate(_rate);
      const _kycApproved = await state.tokenSaleInstance.methods
        .checkKyc(state.account.address)
        .call();
      setKycApproved(_kycApproved);
    }
  };
  return (
    <Container style={{ margin: "2.5rem auto", width: "80%" }}>
      {kycApproved ? (
        <Form onSubmit={onSubmit}>
          <h4 className="text-center">Enter Amount of Tokens to Buy</h4>
          <h5 className="text-center">Rate of Token is {rate}</h5>
          <Form.Control
            type="number"
            value={tokenAmount}
            onChange={(e) => setTokenAmount(e.target.value)}
            style={{ width: "50%", margin: "1rem auto" }}
          />
          <Button
            style={{ margin: "1rem auto", display: "block" }}
            type="submit"
            variant="primary"
          >
            Buy Token
          </Button>
        </Form>
      ) : (
        <>
          <Alert variant="danger">
            You are not KYC whitelisted, you needed to be KYC approved in order
            to buy tokens. Please contact the owner to get KYC whitelisted.
          </Alert>
        </>
      )}
    </Container>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps)(BuyToken);
