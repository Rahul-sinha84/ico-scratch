import React, { useState } from "react";
import {
  Container,
  Form,
  FloatingLabel,
  Button,
  Row,
  Col,
} from "react-bootstrap";
import { connect } from "react-redux";

const HandleKyc = ({ state }) => {
  const [approveText, setApproveText] = useState("");
  const [revokeText, setRevokeText] = useState("");

  const toApprove = async (e) => {
    e.preventDefault();
    if (!approveText) return alert("Enter valid address !!");
    //approving address
    await state.tokenSaleInstance.methods
      .approveKyc(approveText)
      .send({ from: state.account.address })
      .then((res) => {
        setApproveText("");
      });
  };

  const RevokeKyc = async (e) => {
    e.preventDefault();
    if (!revokeText) return alert("Enter valid address !!");
    //revoking address
    await state.tokenSaleInstance.methods
      .revokeKyc(revokeText)
      .send({ from: state.account.address })
      .then((res) => {
        setRevokeText("");
      });
  };
  return (
    <Container>
      <Row style={{ marginTop: "5rem" }}>
        <Col>
          <h3 className="text-center mb-5">Enter Address to Approve KYC</h3>
          <Form onSubmit={toApprove}>
            <FloatingLabel
              controlId="floatingInput"
              label="Account Address"
              className="mb-3"
              style={{ width: "70%", margin: "0 auto" }}
            >
              <Form.Control
                value={approveText}
                onChange={(e) => setApproveText(e.target.value)}
                type="input"
                placeholder="0x0...."
              />
            </FloatingLabel>
            <Button
              style={{ margin: "2rem auto", display: "block" }}
              type="submit"
              variant="primary"
            >
              Approve KYC
            </Button>
          </Form>
        </Col>

        <Col>
          <h3 className="text-center mb-5">Enter Address to Revoke KYC</h3>
          <Form onSubmit={RevokeKyc}>
            <FloatingLabel
              controlId="floatingInput"
              label="Account Address"
              className="mb-3"
              style={{ width: "70%", margin: "0 auto" }}
            >
              <Form.Control
                value={revokeText}
                onChange={(e) => setRevokeText(e.target.value)}
                type="input"
                placeholder="0x0...."
              />
            </FloatingLabel>
            <Button
              style={{ margin: "2rem auto", display: "block" }}
              type="submit"
              variant="danger"
            >
              Revoke KYC
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = (state) => ({ state: state.contract });

export default connect(mapStateToProps)(HandleKyc);
