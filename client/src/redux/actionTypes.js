export default {
  token: "TOKEN_CONTRACT",
  tokenSale: "TOKENSALE_CONTRACT",
  owner: "IS_OWNER",
  money: "MONEY_INSTANCE",
  web3: "WEB3",
  account: "ACCOUNT_DETAILS",
};
