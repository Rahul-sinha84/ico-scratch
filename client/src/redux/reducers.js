import { combineReducers } from "redux";
import type from "./actionTypes";

const initialState = {
  tokenInstance: {},
  tokenSaleInstance: {},
  isOwner: false,
  moneyInstance: {},
  web3: {},
  account: {
    address: "",
    tokenBalance: null,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case type.token: {
      return { ...state, tokenInstance: action.payload };
    }
    case type.tokenSale: {
      return { ...state, tokenSaleInstance: action.payload };
    }
    case type.owner: {
      return { ...state, isOwner: action.payload };
    }
    case type.money: {
      return { ...state, moneyInstance: action.payload };
    }
    case type.web3: {
      return { ...state, web3: action.payload };
    }
    case type.account: {
      return { ...state, account: action.payload };
    }
    default: {
      return state;
    }
  }
};

export default combineReducers({
  contract: reducer,
});
