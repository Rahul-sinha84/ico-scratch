import type from "./actionTypes";

export default {
  changeToken: (data) => ({ type: type.token, payload: data }),
  changeTokensale: (data) => ({
    type: type.tokenSale,
    payload: data,
  }),
  changeMoney: (data) => ({ type: type.money, payload: data }),
  changeOwner: (data) => ({ type: type.owner, payload: data }),
  changeWeb3: (data) => ({ type: type.web3, payload: data }),
  changeAccount: (data) => ({ type: type.account, payload: data }),
};
