pragma solidity ^0.8.0;

import "./Token.sol";
import "./kycContract.sol";
import "./MoneyCollector.sol";
contract TokenSale is KycContract{
    Token tokenContract;
    MoneyCollector moneyCollectorContract;
    uint public rate;
    uint public tokenSold;
    //total wei collected
    uint private weiCollected;


    //events
    event TokenPurchase(address _buyer, uint _token); 
    event ForwardFund(address _to, uint _amount);

    constructor(Token _tokenContract, uint _rate, MoneyCollector _moneyCollector) {
        //rate should not be zero
        require(_rate != 0, "Rate should not be zero !!");

        tokenContract = _tokenContract;
        moneyCollectorContract = _moneyCollector;
        rate = _rate;
    }

    //modifiers
    modifier tokenShouldBeThere() {
        //checks whether the token limit reached or not
        require(tokenContract.totalSupply() - (tokenSold + noOfToken(msg.value)) > 0, "Token limit reached, no token left !!");
        _;
    }

    //get total funds
    function totalFund() public view returns(uint) {
        return weiCollected;
    }
    function noOfToken(uint _value) private view returns(uint) {
        uint  value = _value / rate;
        return value;
    }


    function buyToken(uint _token) public payable tokenShouldBeThere returns(bool) {
        //checking kyc approval
        require(checkKyc(msg.sender), "Not KYC approved !!");
        require(_token == noOfToken(msg.value),"Amount paid is not equal to the requested token");
        require(tokenContract.balanceOf(address(this)) >= noOfToken(msg.value), "Not enough token available !!");
        require(tokenContract.transfer(msg.sender, _token), "Some error occurred !!");

        tokenSold += _token;       

        weiCollected += msg.value; 

        //forwarding funds
        forwardFunds();
        emit TokenPurchase(msg.sender, _token);
        emit ForwardFund(address(moneyCollectorContract), msg.value);

        return true;
    }

    //forward funds to money collector contract
    function forwardFunds() private {
        //forwarding the collected amount to different contract
        payable(address(moneyCollectorContract)).transfer(msg.value);
    }
    //for external payment
    receive() external payable {
        buyToken(noOfToken(msg.value));
    }
}