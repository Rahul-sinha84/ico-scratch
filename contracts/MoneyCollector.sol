pragma solidity ^0.8.0;

contract MoneyCollector {
    address payable public owner;

    //modifiers
    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner required !!");
        _;
    }

    constructor() {
        owner = payable(msg.sender);
    }

    //returning the total balance in contract
    function totalMoney() public view onlyOwner returns(uint) {
        return address(this).balance;
    }

    //withdraw money from contract
    function withdrawMoney(uint _money) public onlyOwner {
        uint _totalMoney = address(this).balance;
        require(_totalMoney >= _money, "Not enough funds !!");

        //some code to mutate the functionality of the ICO

        //transferring money to owner of ICO
        owner.transfer(_money);
    }   

    //receiving ether 
    receive() payable external {

    }
}