pragma solidity ^0.8.0;

contract KycContract {
    address public owner;

    mapping(address => bool) private isKyc;
    uint private totalKycs;
    
    //events
    event KycApproved(address _address, string _message);

    constructor() {
        owner = msg.sender;
    }

    //modifier for owner
    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner required !!");
        _;
    }

    //check for owner
    function isOwner(address _owner) public view returns(bool) {
        return owner == _owner;
    }
    function checkKyc(address _user) public view returns(bool) {
        return isKyc[_user];
    }

    //get total Kycs
    function totalKyc() public view onlyOwner returns(uint) {
        return totalKycs;
    }

    function approveKyc(address _user) public onlyOwner {
        require(!isKyc[_user], "Already Kyc approved !!");
        isKyc[_user] = true;
        totalKycs++;
        emit KycApproved(_user, "You are now KYC approved");
    }
    function revokeKyc(address _user) public onlyOwner {
        require(isKyc[_user], "Not Kyc approved !!");
        isKyc[_user] = false;
        totalKycs--;
        emit KycApproved(_user, "Your KYC approval is now revoked");
    }
}