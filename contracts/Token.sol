pragma solidity ^0.8.0;

contract Token {
    //set the total number of tokens
    uint private totalTokens;
    string private tokenName;
    string private tokenSymbol;

    //storing all tokens per user
    mapping(address => uint) private owners;
    //allowance for user
    mapping(address => mapping(address => uint)) private allowedOwners;

    //events
    event Transfer(address _from, address _to, uint _value);
    event Approval(address _owner, address _spender, uint _value);

    constructor(string memory _name, string memory _symbol, uint _totalTokens) {
        tokenName = _name;
        tokenSymbol = _symbol;
        totalTokens = _totalTokens;
    }
    function totalSupply() public view returns(uint) {
        return totalTokens;
    }
    function name() public view returns(string memory) {
        return tokenName;
    }
    function symbol() public view returns(string memory) {
        return tokenSymbol;
    }

    //getting the balance of a user
    function balanceOf(address _address) public view returns(uint) {
        return owners[_address];
    }
    //allowed value to transfer
    function allowance(address _owner, address _spender) public view returns(uint) {
        return allowedOwners[_owner][_spender];
    }

    //give allowance
    function approve(address _spender, uint _value) public returns(bool) {
        allowedOwners[msg.sender][_spender] = _value;

        emit Approval(msg.sender, _spender, _value);
        return true;
    }
    
    //transferring the tokens
    function transfer(address _to, uint _value) public returns(bool){
        require(balanceOf(msg.sender) >= _value, "Insufficient balance !!");

        owners[msg.sender] -= _value;
        owners[_to] += _value;

        //emitting event
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    //transferring tokens on behalf of someone
    function transferFrom(address _from, address _to, uint _value) public returns(bool) {
        require(balanceOf(_from) >= _value, "Insufficient balance !!");
        require(allowedOwners[_from][msg.sender] >= _value, "Transfering value is more than allowed value !!");

        owners[_from] -= _value;
        owners[_to] += _value;

        allowedOwners[_from][msg.sender] -= _value;

        emit Transfer(_from, _to, _value);
        return true;
    }

    //initial transfer of tokens
    function initialTransfer(address _to, uint _value) public returns(bool) {
        
        require(_to != address(0), "Cannot transfer tokens to 0th account");

        owners[_to] += _value;

        emit Transfer(address(this), _to, _value);

        return true;
    }
}