var Token = artifacts.require("./Token.sol");
const TokenSale = artifacts.require("./TokenSale.sol");
const MoneyCollector = artifacts.require("./MoneyCollector.sol");

module.exports = async function (deployer) {
  const name = "Token";
  const symbol = "TKN";
  const totalToken = 100000000;
  const rate = 1;
  await deployer.deploy(Token, name, symbol, totalToken);
  await deployer.deploy(MoneyCollector);
  await deployer.deploy(TokenSale, Token.address, rate, MoneyCollector.address);

  //transeferring all tokens
  let tokenInstance = await Token.deployed();
  await tokenInstance.initialTransfer(TokenSale.address, totalToken);
};
